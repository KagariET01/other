# 電腦雜談
今天要教的主題比較多，但都是十分鐘就能教完的那種。  
但沒有10秒鐘可以教完的，如果你想要10秒鐘教程，請找桐人。~~48763~~  

# 超實用快速鍵列表
這些快速鍵真的一定要學起來  

`[Win]`+`[L]` ~~一鍵~~兩鍵鎖電腦  
~~一鍵~~兩鍵鎖住自己電腦，避免其他人惡搞  
***離開電腦前記得按***  
（精華：不知道是哪個人趁我不在偷搞我DC）
  
![社長被惡搞](joke_1.png)  


`[Ctrl]`+`[C]` 複製  
`[Ctrl]`+`[V]` 貼上  
這兩個按鍵大家應該都知道，那這個呢？  
`[Win]`+`[V]` 剪貼簿紀錄(Windows only)  
![Alt text](image-12.png)  
尼可以按很多次`[Ctrl]`+`[C]`，然後使用剪貼簿紀錄來貼上
    
下圖為超專業的截圖錯誤示範  
![](hahaha.png)  
`[Win]`+`[Print Screen]` 截圖  
圖片會被保存到[圖片/Screenshots](%AppData%\..\..\Pictures\Screenshots)  
p.s.`[Print Screen]`鍵通常在鍵盤的右上角 ~~，找不到？請無視數字鍵盤，然後再試一次~~  
`[ET01]`~~老師，我的鍵盤沒有[Print Screen]按鍵~~***(真的)***  
`[Win]`+`[Shift]`+`[S]` 跳出截圖工具(Windows only)  
你可以框選你想截圖的範圍，擷取到的圖會存到剪貼簿裡，按`[Ctrl]`+`[V]`可以把他們叫出來  
或是你可以在[剪取工具]中設定自動儲存  

`[Alt]`+`[Tab]` 切換視窗  
提示，你可以按住`[Alt]`鍵，然後按多次`[Tab]`鍵來切換更早之前的視窗  
p.s.不要拿這個快速鍵做奇怪的事  
`[Win]`+`[<-]` 將視窗靠左  
讓你的視窗靠左對齊，你還可以選另一個應用程式填補右邊的空白  
`[Win]`+`[->]` 將視窗靠右  
用法同上，但這是讓視窗靠右對齊  
你可以使用這個功能一邊查資料，一邊做校必報告  
`[Shift]`+`[空白鍵]`切換全形、半形  
製作校必報告,標點符號需要打全形,這一行是錯誤示範,(請勿模仿)!  
ｐ．ｓ． 全形模式中，英打輸入也會變大，請記得切回半形輸入後再輸入英文，這一行也是錯誤示範。  
\` + 任意符號 輸入全形符號



# 如何挑筆電
最簡單的方法就是直接問大老(不是我)  
但要問也不要只問這句：`請問你推薦我什麼筆電`  
不只我會傻眼，[連YTR都很無奈](https://youtu.be/w9lRHkB8tgU?t=262)  
請報上需求及預算，我們才找得到一台適合你的筆電  
(目前沒有任何一台筆電是100%適合所有人的)  
標準問法範例：  
```txt
電神，能推薦我一台筆電嗎？
品牌愛好：AC牌最好，WA牌次佳，不要選TLE的
類型：文書(or創作or電競)
使用需求：撰寫報告、偶爾剪輯影片
希望跑得動Yourcraft(全低畫質、不裝第三方內容、60FPS)
硬碟需求：最好1TB
預算：最高2萬
```
雖然很長，但這才是標準的提問方式  
  
`[Student]`老師，我社恐不敢請教大老  
`[ET01]`那就先從選陣營開始講起  

## Windows 或是 Mac？
又或者說
### What is Windows?
Windows電腦，就是各位現在在用的電腦(自己帶電腦的除外)  
優點：  
種類多，具有多樣性，可高度客製化  
軟體支援度高，還可以玩3A大作(Mac沒辦法玩近幾年出的3A大作)  
缺點：  
種類多，難以比較、選擇，對猶豫症患者極度不友善  
續行偏低，詳細可以看[我们找到了Windows电脑续航差的原因！苹果M2深度分析 by 极客湾Geekerwan](https://youtu.be/Z0tNtMwYrGA)  
推薦使用者：  
遊戲需求，需要專業軟體，未來會想升級電腦硬體，習慣學校電腦系統  

### What is Mac?
Mac電腦，有蘋果logo的電腦都叫Mac電腦  
優點：  
種類少，客製化選項較少，選擇起來比較簡單  
功耗低，省電，風扇很安靜，續行強  
輕薄，和其他蘋果產品可做到無縫間容  
缺點：  
接孔少，`MacBook Pro`沒有配備`USB-A`接孔，需要加購USB HUB  
不能跑多數3A大作(需要裝模擬器)  
種類少，不能對特殊需求作訂做(`MacBook Air`和`MacBook Pro`二選一)  
記憶體、硬碟買斷制，沒有升級空間  
推薦使用者：  
使用iPhone,Apple Watch...等Apple產品  
長時間離電使用(外出使用)、沒有太大的遊戲需求(或者已確認該遊戲支援Mac)  
對噪音敏感、需要使用Mac獨佔軟體(如GarageBand, Final Cut Pro)  

## 如何看懂各種電腦配備
避坑須知：建議不要看官方的宣傳圖，請直接看最下方的"產品規格"資訊  
p.s. CPU和GPU的介紹主要放在`Intel CPU`和`NVIDIA`顯卡，AMD系列的晶片我不太熟，無法提供教程  
p.s. 本人不是Mac用戶，無法提供Apple電腦的相關資訊

### CPU
CPU是整個電腦的核心  
以這顆Intel CPU為例：`Intel®Core™ i5-12450H Processor 2.0 GHz (12M Cache, up to 4.4 GHz, 8 cores)`  
前面的`Intel®Core™`代表這顆CPU是`Intel`公司設計，`Core`系列  
p.s. `Intel®Core™`系列是最常見的CPU系列之一  
  
在後面的`i5-12450H`可以看做型號名  
  
前面的`i5`代表等級，有分`i3`,`i5`,`i7`,`i9`  
`i3`代表當代最低階的，但相對比較便宜、(理論上)耗電量低  
`i9`代表當代最高階的，但相對比較貴、 ~~(理論上)~~ 耗電量高  
  
後面的`12450H`要把它拆成`12`,`450`,`H`  
`12`代表CPU次代，像這一顆就是第12代，扣除掉英文字母和SKU碼，就可以獲得此值  
`450`代表SKU數字，數值越高，功能通常越多，固定是3位數    
`H`代表尾碼，可能有，可能沒有，但這不再今天的討論範圍，也比較不重要(對我來說)  
  
~~後面的通常不是重點，可以略過~~  
~~其他系列的CPU比較冷門，我也不懂，就放過我把~~

練習題：請告訴我`i9-12900H`是第幾代CPU、定位如何。  

#### 如何選擇??
盡量選第11、12代or以上  
輕度辦公：`i3`,`i5`  
剪輯、創作：`i7`,`i9`  
電競：`i7`,`i9`  
p.s. 這邊只是建議，不代表適合每個人，購買前建議上網尋找評測資料  
p.s. 筆電的CPU無法拆開升級，為買斷制  
### GPU
顯示卡主要用於3D模型繪製、光線追蹤，如果有玩3D遊戲的，畫質習慣開很高的人，你需要一張好的顯卡  
我們今天也只介紹`GeForce`系列顯卡做舉例  
`NVIDIA GeForce RTX 3050 Ti Laptop GPU`  
最後面的`Laptop GPU`明顯是指這是一張for筆電的GPU  
前面的`NVIDIA Geforce`是商家名、系列名  
重點來了，`RTX 3050 Ti`  
`RTX`其實也算是指系列，代表這張顯卡支援光追  
另外還有`GTX`系列，就是沒有光追的~~高階~~顯卡  
`3050`中的`30`代表這是第3代，`50`代表他的階級  
階級最低`50`(only NB),`60`  
最高`90`  
但通常`70`,`80`左右就能滿足大部分的人的需求了，`50`的性能也不算太差，不要為了`90`顯卡而多花`5000$`，有點浪費

而後面的`Ti`代表他是升級款，比非`Ti`版本強

#### 如何選擇
輕度辦公：免  
電競、剪輯、創作：`RTX 3060` or up  
AI訓練：`RTX4080`，或直接組桌機
p.s. 這邊只是建議，不代表適合每個人，購買前建議上網尋找評測資料  
p.s. 部分筆電的GPU可以用外接顯卡的方式升級  
### 記憶體
注意，這邊的記憶體和儲存空間(如：隨身碟)是不同的東西  
大記憶體可以讓你一次開多個應用程式，或者處理更複雜的工作(如：特效剪輯、AI運算)  
8GB*2 LPDDR5 on board  
最大容量16GB  
代表這電腦傭有`8+8`的記憶體大小  
p.s. `8+8`的理論性能會比`16+0`還要高  
`on board`代表記憶體死焊在主機板上，不可升級  
#### 如何選擇
輕度辦公：`8GB`  
電競：`16GB`  
剪輯、創作：`32GB` or 直接組大桌機，`64GB`  
p.s. 這邊只是建議，不代表適合每個人，購買前建議參考軟體建議的規格  
p.s. 部分筆電的RAM可以透過開蓋來更換、升級

現在請大家查看[這台筆電](https://www.asus.com/tw/laptops/for-creators/zenbook/zenbook-pro-14-duo-oled-ux8402/)，並告訴我他的最低規格中，CPU使用第幾代、顯示卡規格。  
第一個回答的...(看講師當天有沒有帶臨時)

## 綜合統整？
### Mac陣營：  
有專業需求，如大量特效剪輯，則建議選擇`MacBook Pro`  
想要有便攜性，沒有太大的效能需求，建議選擇`MacBook Air`  
背包空間小的話，則建議13吋左右，背包空間大的再考慮15吋  
### Windows陣營：
輕度辦公  
> CPU：11代i5  
> GPU：免  
> RAM：8GB  
電競  
> CPU：12代i7 or up  
> 


# 真去背？假去背？
你知道網路上有很多假去背圖ㄇ？  
以這張圖為例  
![Alt text](image-1.png)  
除了繪圖軟體，不然去背圖不會存在後面的網格  
標準的去背圖會同時滿足以下事情  
- 副檔名為`.png`  
- 沒有網格  
- 拖動圖片，背景不會跟著被拖動  

那請告訴我：下面這張圖是否為去背圖  
![Alt text](image-6.png)  

`[Student]`那如果只找到假去背圖，怎麼辦？  
`[ET01]`可以使用`PowerPoint`去背功能  
![Alt text](image-3.png)  
被標紫底的範圍都會被去除掉  
![Alt text](image-4.png)  
完成，可以對該張圖片`右鍵`->`另存成圖片`  
p.s. 附檔名請選`.png`  
![Alt text](image-5.png)  


# 基本Markdown語法教學
`Markdown` ，簡稱 `md` ，算程式語言，也不算是  
可支援部分 `HTML` 語法，還新增一些淺顯易懂的語法表示
md語法可以參考 [基礎md語法大全](https://hackmd.io/@ET01/MDbasic#/)  
或[HackMD使用教學](https://hackmd.io/c/tutorials-tw/%2Fs%2Ftutorials-tw)  

下列網站&APP都支援Markdown語法  
- VS code(支援度最高)(但有時候還是會碰到bug)(不建議用.md形式撰寫HTML)
- Github(支援所有基礎MD語法，半支援HTML語法)
- HackMD(支援所有基礎MD語法)(最佳筆記軟體) ~~(還支援其他有的沒的的語法)~~ 
- Discord(支援度不高，但該有的都有)

# 快速電腦修復
電腦是個高精密的產品，只要出現一點差錯就會藍屏死雞  
  
![blue\_screen](https://raw.githubusercontent.com/KagariET01/other/main/zh_TW/電腦雜談/blue_screen.jpg)  
圖片取自[windows10蓝屏怎么办？如何修复常见的Windows 10蓝屏错误 - 知乎](https://zhuanlan.zhihu.com/p/86423062)
  
阿放錯了，是這張  
  
![dead\_chicken](https://raw.githubusercontent.com/KagariET01/other/main/zh_TW/電腦雜談/chicken.jpg)
圖片取自[影片:如何燒雞皮才會脆？](https://www.ytower.com.tw/movie/play.asp?id=M-2411)  
  
如何快速處理這種狀況？
1. 重新開機解決80%的問題(但也有很多問題是重啟解決不了的)
2. 重灌系統解決90%的問題
3. 重買電腦解決99.999999999%的問題
  
剩下0.000000001%的問題呢，如果你是在一頁式詐騙網站買的話…。  
  
`[Student]`但後面兩個方式都好麻煩歐，有沒有直接對症下藥的解方  
`[ET01]`答案:問Google,ChatGPT  
但問答要有技巧，以下是非常專業的錯誤示範
```txt
我的筆電壞了
```
`工程師`你筆電壞掉是指不能開機、鍵盤不可用、還是...  
## 如何問問題
1. 說明你遇到的困難（電腦無法開機）
2. 電腦型號、規格（你也可以直接貼網址）
3. 把所有你觀察到的東西記錄下來（充電時指示燈有亮）
4. 電腦壞掉之前你做了甚麼事（CPU超頻）
5. 把過程錄影下來
6. 想到什麼就寫什麼，寫多少算多少，但不要廢話

最後整合如下
```txt
我的電腦無法開機，充電時指示燈有亮，電腦壞掉前，我有對CPU超頻
這是我買的電腦：https://www.youtube.com/watch?v=dQw4w9WgXcQ
這是完整開機影片：https://www.youtube.com/watch?v=ZZ5LpwO-An4
```
把這一串留言貼給專家，專家能快速了解問題所在

`[Student]`我要自己排錯，我社恐不敢聯絡工程師。  
方法如下
1. 列出所有可能使電腦故障的變因（螢幕、主機板、硬碟...）
2. 逐一檢查每個變因（將硬碟放進硬碟讀取器檢查）
3. 正常的零件移除名單（硬碟讀取正常，嫌疑排除）
4. 維修剩下的變因（最後發現是主機板壞掉，更換主機板）
5. 確認問題排除（是：結束，不是：回步驟1）
  
`[Student]`老師，我想不出有什麼變因會導致電腦壞掉  
`[ET01]`Google, ChatGPT搜尋、詢問:引起電腦壞掉的所有可能變因

# How to Git
Git是管理程式碼最有效的軟體  
非常建議校隊同學練習使用Git  

首先先註冊[GitHub](https://github.com/signup)    
![Sign up](https://raw.githubusercontent.com/KagariET01/other/main/zh_TW/電腦雜談/signup.png)  
  
你應該會在你的郵件箱裡面找到你的驗證碼，請將該驗證碼填入最下方的輸入欄  
雖然畫面看起來很終端機，但這不是終端機  
  
建立倉庫  
點右上角的`+`號\>New repository  

![New repository](https://raw.githubusercontent.com/KagariET01/other/main/zh_TW/電腦雜談/Newrepos.png)  
  
接著設定下面這些東西  
  
![New repository](https://raw.githubusercontent.com/KagariET01/other/main/zh_TW/電腦雜談/Newrepos2.png)
  
那要如何使用Github倉庫？  
  
正規方法：  
~~先`sudo apt update`，再來`sudo apt install git`，再來`git clone <網址here>`  
要將所有變更過的檔案加入暫存器，使用`git add .`  
將變更上傳到本地倉庫，使用`git commit`  
將變更上傳到網路上，使用`git push`  
把網路上的變更下載下來，使用`git pull`~~  
......

`[Student]`這也太複雜了把，有沒有更腦殘的方法  
`[ET01]`诶，還真的有
  
首先先到這邊載UI設定軟體<https://desktop.github.com/>  
下載完後點開，它會自動幫你安裝Github Desktop並開啟  
你會看到的畫面如下，我們點擊左邊的`Sign in to GitHub.com`登入
![](gitapp_01.png)  
他會自動打開瀏覽器，如果他叫你登入就登入  
![](gitapp_02.png)  
它可能會問你是否授權給GitHub Desktop，我們就點確定  
因為講師之前有用過這個軟體，所以他不會要求我授權，我也沒辦法截圖給你們看  
再來它會讓你確認Git的設定檔，這邊我們用GitHub預設的設定就好了  
p.s.下面的Email僅供GitHub辨識，你沒辦法寫email給該地址
![](gitapp_03.png)  
這樣就設定完Git的帳號設置了  
  
接下來要把剛剛建立的Git倉庫搬到電腦上，我們才能操作辣個倉庫  
先在右邊選擇你的Git倉庫，接著點下面的`Clone ......`  
![](gitapp_04.png)  
它會跳出一個確認框，上面的網址不要動，點下面的Choose，選擇你想要把倉庫放在哪裡(初學者建議選擇桌面)，最後按Clone  
![](gitapp_05.png)  
全部設定完成，你會在桌面發現一個新的資料夾，資料夾名稱和你的Git倉庫名稱一樣  
那個資料夾就是你的Git倉庫  
p.s.資料夾裡面有Git倉庫的設定檔，預設是隱藏的，那些東西不能刪。  
試著在資料夾裡面新增檔案，並在裡面寫一些東西  
你也可以把你刷題的code複製到資料夾裡  
新增完檔案後，App應該會顯示如下
![](gitapp_06.png)  
左邊為被修改的檔案列表，點擊後會在右邊顯示檔案差異  
你可以在左下角撰寫更改紀錄，轉寫時請記住幾個原則：淺顯易懂、簡潔有力  
確定沒問題之後，按左下角的Commit，就能把檔案送到本機Git倉庫  
![](gitapp_06.png)  
但它只會推送到本地倉庫，你必須手動push到GitHub.com的線上倉庫  
按Push按鈕就可以把變更上傳到GitHub.com  
![](gitapp_07.png)  
你可以在你的GitHub看到你剛剛上傳的內容  
![](gitapp_08.png)  
嘗試編輯你剛剛建立的檔案，他會顯示你做了那些變更  
![Alt text](image-7.png)  
再次把這些變更上傳到github上面  
p.s. commit,push,pull...等功能在vs code 上面也有歐  
![Alt text](image-11.png)  

git可以說是檔案時光機，所有變更、版本都會記錄在git倉庫裡面，即使你不小心改壞檔案，也只需要瀏覽紀錄，就可以把檔案救回來，重點是這些功能完全免費  
![Alt text](image-8.png)  
![Alt text](image-10.png)  
![Alt text](image-9.png)  
恭喜你找到舊的檔案  
因為難度的關係，講師無法講解Git的其他主要功能，如：分支控制(branch)...  
有興趣的可以找[ET01](https://kagariet01.github.io/about)，[Git進階雜談](.https://raw.githubusercontent.com/KagariET01/other/main/zh_TW/電腦雜談/Git進階雜談/README.md) 或 Google  

