#	`HTML` basic
對，今天要教的是HTML  

#	What is HTML?
`HTML`是一種用於設計網頁的程式語言。  
時間關係，我們不會把所有`HTML`的功能教完，有興趣的同學可以自行上[W3Schools](https://www.w3schools.com/html/default.asp)學習。

# Before start, Let's Git!
為何要學`HTML`，因為我們要寫網頁給其他人看  
這個網頁建議好好做，可以放 ***學習歷程***  
[不知道是哪個怪人直接用HTML肝作業，還順便放學習歷程](https://kagariet01.github.io/blog/?p=h7)  
本次社課以github.io的免費空間服務來架設網頁  
  
0. 登入<https://github.com>  
![Alt text](image.png)  
![Alt text](image-1.png)  
1. 建立名為`<your username here>.github.io`的倉庫  
![Alt text](image-2.png)  
![Alt text](image-3.png)  
你的`username`是`48763`，就建立一個名為`48763.github.io`的倉庫  
2. `git clone`到桌面上  
忘記怎麼做？[see me](../電腦雜談/README.md/#how-to-git)  
btw 記得選`username.github.io`的倉庫，不要選成你上次建立的倉庫  
3. 點開該倉庫，並新增名為`index.html`的檔案(注意附檔名)  
![Alt text](image-4.png)  
and now `let you try try`
# How to start?
現在對`index.html`點右鍵>使用Notepad++編輯  
並輸入下面的`HTML`基礎模板  
```html
<!DOCTYPE html>
<html>
	<head>
	</head>

	<body>
	</body>
</html>
```
`html`的基礎結構如下：
```html
<tagname_here>something here</tagname_here>
```
你可以把`<tagname_here>`視為左括號`(`  
`</tagname_here>`視為右括號`)`  
你可以把`<tagname_here></tagname_here>`當作一個箱子，裡面可以放方塊、箱子...  
`tagname_here`代表裡面包的東西的種類  
ex: `body`包主程式，`head`包網站設定  
所以上面的程式碼可以這樣理解  
```txt
<!DOCTYPE html>
(
	(
	)

	(
	)
)
```
而最上面的`<!DOCTYPE html>`只是用來標示這份檔案使用的程式語言  
這一行很重要，別忘記加，因為電腦很笨  
By the way, 編輯完後，***請記得儲存***，接著對檔案雙擊左鍵點開，你可以在瀏覽器看到結果  
and now `let you try try`  

# Hello World!
如題，我們要寫`Hello World!`  
新元素：`<p></p>`代表段落，你可以在這裡面寫東西，一個`<p></p>`代表一個段落  
例如`<p>text here</p>`，網頁就會顯示`text here`  
回想一下`<body></body>`用來包主程式  
答案就出來了  
```html
<!DOCTYPE html>
<html>

	<body>
		<p>Hello World!</p>
	</body>

</html>
```
![Alt text](image-6.png)  
p.s. 別忘記儲存、瀏覽器重新整理(`F5`)，才能看到變更  
p.s. 特殊字元(`<>&'"`)請參考[HTML的特殊字元](./HTML的特殊字元.md)  
and now `let you try try`  
# 換行
有一行`Hello World!`了，現在就來5行`Hello World!`  
要在`HTML`換行，需要使用`<br>`  
記得，`<br>`是單純一個元素而已，裡面不能包東西，所以沒有`</br>`  
你可以把它想像成一個方塊  
答案出來了  
```html
<!DOCTYPE html>
<html>

	<body>
		<p>
			Hello World!<br>
			Hello World!<br>
			Hello World!<br>
			Hello World!<br>
			Hello World!
		</p>
	</body>

</html>
```
![Alt text](image-7.png)  
那`\n`可以在`HTML`換行嗎？或者我可以省略`<br>`嗎？你可以試試看  
p.s. 儲存+重整(`F5`)，才能看到變更  
and now `let you try try`  

# 標題
順帶一提，有人真的試`\n`會發生什麼事嗎？  
有試的人...(看講師當天有沒有帶臨時過來)  
使用`<h1></h1>`~`<h6></h6>`可新增標題  
類似於`MarkDown`的標題`#`  
```md
# <h1></h1>
## <h2></h2>
### <h3></h3>
#### <h4></h4>
##### <h5></h5>
###### <h6></h6>
####### 同樣沒有<h7></h7>
```
使用方法同`<p></p>`  
文字會加粗加大  
ex:  
```html
<!DOCTYPE html>
<html>

	<body>
		<h1>HTML basic</h1>
		<h2>Hello World!</h2>
		<p>
			Hello World!<br>
			Hello World!<br>
			Hello World!<br>
			Hello World!<br>
			Hello World!
		</p>
		<h2>Hello 尖叫ㄐ</h2>
		<h3>Hello 尖叫ㄐ</h3>
		<h4>Hello 小ㄌㄌ</h4>
		<h5>Ststone</h5>
		<h6>linlinorz</h6>
		<h7>沒有h7</h7>
	</body>

</html>
```
![Alt text](image-8.png)  
and now `let you try try`  

# Title
話說，有誰有抬頭看頁籤名稱  
![Alt text](image-5.png)  
`index.html`，好歐  
我們使用`<title></title>`來設定這個頁籤名稱  
請注意，`<title></title>`需要放在`<head></head>`裡面  
ex:  
```html
<!DOCTYPE html>
<html>
	<head>
		<title>尖叫ㄐ的網頁</title>
	</head>
	
	<body>
		<p>...</p>
	</body>
</html>
```
![Alt text](image-9.png)  
and now `let you try try`

# Note
寫註解，讓你可以更好理解自己寫的code  
使用`<!--Note here-->`撰寫註解  
Note here不會顯示  
```html
<!DOCTYPE html>
<html>

	<body>
		<!--
			<h1>HTML basic</h1>
			<h2>Hello World!</h2>
			<p>
				Hello World!<br>
				Hello World!<br>
				Hello World!<br>
				Hello World!<br>
				Hello World!
			</p>
			<h2>Hello 尖叫ㄐ</h2>
			<h3>Hello 尖叫ㄐ</h3>
			<h4>Hello 小ㄌㄌ</h4>
			<h5>Ststone</h5>
			<h6>linlinorz</h6>
			<h7>沒有h7</h7>
		-->
	</body>

</html>
```
![Alt text](image-10.png)  
and now `let you try try`  

# 超連結
要在`HTML`裡插入連結，不能直接貼網址  
![Alt text](image-11.png)  
在`HTML`，使用`<a></a>`插入連結  
但是這邊的用法不太一樣  
我們要設定`<a></a>`的`attributes`，也就是屬性  
```txt
<tagename_here name="value" name2="value">
</tagname_here>
```
其中的`name="value`就是屬性  
他會包在左標籤中，意即尾端的`</tagname_here>`不用寫屬性  
而`<a></a>`需要加入屬性`href`來指定該串文字要連到的位置  
例如  
```html
<html>
	<body>
		<p>
			我最喜歡的歌：<a href="https://youtu.be/dQw4w9WgXcQ">點我跳轉</a><br>
			<a href="https://youtu.be/dQw4w9WgXcQ">https://youtu.be/dQw4w9WgXcQ</a>
		</p>
	</body>
</html>
```
點擊`點我跳轉`，就會跳轉到我最喜歡的歌  
當然，點下面的網址也可以  
但這個連結最好不要點  
and now `let you try try`  

# Favicon
再次抬頭看上面，我們的網頁還沒有自己的icon  
![Alt text](image-13.png)  
我們使用`<link rel="icon" type="image/x-icon" href="網址here">`來新增icon  
同樣，此行程式碼不用尾元素`</link>`，因為它本身是一個方塊，裡面不能放東西  
此行程式碼也是要放在`<head></head>`裡面
程式碼解析  
```html
<link 
	rel="icon"            note="表示這東西要放在icon欄位上"
	type="image/x-icon"   note="表示這東西的類型是icon"
	href="網址here"     note="icon檔案的位置"
>
```
而`網址here`則是填網路上的圖片URL，或是圖片的檔名(圖片和`index.html`必須在同一個資料夾裡)  
例如：我的檔案結構如下  
> `kagariet01.github.io`  
>> `index.html`  
>> `image.png`  

則我可以在`網址here`放入`image.png`  
```html
<!DOCTYPE html>
<html>
	<head>
		<link rel="icon" type="image/x-icon" href="image.png">
		<title>ET01_web</title>
	</head>
	
	<body>
		<p>...</p>
	</body>
</html>
```
![Alt text](image-14.png)  
p.s. 把該頁面加入書籤，書籤也會顯示該icon  
![Alt text](image-15.png)  
and now `let you try try`

# 圖片
`<img src="網址here" alt="如果圖片載入不了，則顯示這裡的訊息">`代表圖片  
`<img>`同樣不用加尾元素`</img>`
`<img>`解構如下  
```html
<img
	src="圖片網址here"
	alt="如果圖片載入不了，則顯示這則訊息，可有可無"
>
```
下面比較`<a></a>`和`<img>`的差別  
|`<a></a>`|`<img>`|
|:-:|:-:|
|需要尾元素|不需要|
|設定`href`屬性|設定`src`屬性|

下面示範  
```html
<!DOCTYPE html>
<html>
	<head>
	</head>
	
	<body>

		<p>
			這是一個很可愛的小ㄌㄌ<br>
			<img 
				src="https://avatars.githubusercontent.com/u/66681962"
				alt="小ㄌㄌ的照片"
			>
			<br>
			這邊又有一張小ㄌㄌ的圖片<br>
			<img 
				src="image.png"
				alt="小ㄌㄌ的照片"
			>
			<br>
		</p>

	</body>
</html>
```
![Alt text](image-16.png)  
and now `let you try try`  

# 上架你的網頁
雖然我們可以在瀏覽器看到自己的作品，但這個作品是本機檔案，其他裝置ex.手機則無法瀏覽這個網頁  
![Alt text](image-17.png)  
回到GitHub Desktop，將我們的程式碼上傳到GitHub  
等2min之後，應該就會在 <https://你的GitHub用戶名.github.io> 看到你的作品  
以社長`ET01`的為例，他的GitHub帳號是`kagariet01`，所以他的連結是  
[https://`kagariet01`.github.io](https://kagariet01.github.io)  
p.s. 上架網頁需要伺服器運算處理，若人潮過多，則處理時間會比較久，請耐心等候  
and now `let you try try`  

# 樣式
剛剛，我們看到的是白底黑字的亮色主題  
但現在，我們要將這個網頁改成黑底白字的暗色主題  
這邊有一個屬性：`style`樣式  
而style裡面可以設定背景色、排版、文字顏色、文字大小、字型...  
而`style`本身也是一種程式碼  
基礎的`style`程式碼  
```html
<tagname_here
	name="value"
	style="
		name: value;
		name2: value;
		name3: value;
	"
></tagname_here>
```
這邊有個例子  
```html
<!DOCTYPE html>
<html>

	<body>
		<p
			style="
				color: red;
			"
		>
			我是紅色的文字
		</p>
		<!---------------->
		<p
			style="
				color: #fff;
				background-color: #000000;
			"
		>
			我是黑底白字的文字
		</p>
	</body>

</html>
```
常用樣式整理表  
|程式|效果|
|:-:|:-:|
|color: `顏色`;|設定文字顏色|
|background-color: `顏色`;|設定背景顏色|
|background-image:url("`網址here`");|設定背景圖片|
|font-size: `大小`;|設定文字大小|
|border: `大小` `樣式` `顏色`;|設定邊框|
|text-align: `文字對齊樣式`;|文字對齊|
|display: `顯示樣式`|顯示樣式|
- `顏色`：`#08f`(紅 綠 藍 0~F) `#02468ace`(紅紅 綠綠 藍藍 透明度 00~FF) `rgb(123,456,789)`(紅,綠,藍 0~255) `rgba(123,456,789,0.87)`(後面的小數為透明度 0~1)
- `大小`：`12px`
- `文字對齊樣式`：`center`置中 `right`靠右 `left`靠左
- `顯示樣式`：`block`預設顯示 `none`不顯示(這個東東在`JS`會用到)

其他內容可以自行上W3school上查看  

and now `let you try try`  
p.s. 離開電腦教室前，別忘記把code上傳到GitHub上歐  
(code上船完畢後，剩下的事情GitHub伺服器會幫忙處理，不用死守著電腦)  
~~水拉，又多一個學習歷程可以水~~  

















# 诶，教完了也
那現在要幹嘛
