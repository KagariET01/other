# 複雜度
資訊競賽是很講究程式的執行時間的，亂寫程式只會吃TLE。  
所以我們需要一個簡單分析時間的方法，在和大家討論的時候也希望有個共通語言能表示。  
## What is 複雜度  
大O函數(Big O notation)，可以用來表示一個複雜度的「上界」  
根據定義，若g(n)的成長速率優於f(n)的話，我們就說f(n) ∈ O(g(n))
雖然複雜度的基本定義絕非僅此，但你現在只需要知道這些  
## What is 時間複雜度
時間複雜度用來比較該程式的效率，我們甚至能用**`時間複雜度`**來推測出這個程式會不會**`TLE`**  
## How to 計算
把所有基本運算都看成是O(1)，根據輸入的變數範圍、大小來訂定出屬於這隻程式的時間複雜度  
找到一個符合的複雜度，把題目給定的範圍代入，即可確認該複雜度會不會吃TLE  
  
對於每分code，我們把所有運算都當成花費一單位時間O(1)  
p.s.這裡的運算是指 `+ - * / % == \< \>`   
最後把所有的迴圈、地回函數、輸入輸出(當作O(1))...的操作全部統整起來，就可以得到此code的時間複雜度，進而得知此份code會不會吃TLE  
  
你可以假設***電腦一次能執行1e8次運算***，即好的複雜度應壓在O(1e8)以內  
`O(1)` \< `O(log n)` \< `O(sqrt(n))` \< `O(n)` \< `O(n log n)` \< `O(n^2)` \< `O(n^3)` \< `O(2^n)` \< `O(n!)`  
  
講了那麼多，不如直接試算看看  
```c++
int a,b,c;
c=a+b;
```
答案：<div style="color: #000;background-color: #000;">O(1)</div>  
```c++
int a,b,c;
cin>>a>>b;
c=a+b;
cout<<c<<endl;
```
答案：<div style="color: #000;background-color: #000;">O(1)</div>  
```c++
int n;
cin>>n;
int a[n];
for(int i=0;i<n;i++){
	cin>>a[i];
}
```
答案：<div style="color: #000;background-color: #000;">O(n)</div>  
```c++
int n;
cin>>n;
int a[n];
for(int i=0;i<n;i++){
	cin>>a[i];
}
int c=0;
for(int i=0;i<n/2;i++){
	c+=a[i];
}
```
答案：<div style="color: #000;background-color: #000;">O(n)</div>  
```c++
int n;
cin>>n;
int a[n];
for(int i=0;i<n;i++){
	cin>>a[i];
}
sort(a,a+n);
```
答案：<div style="color: #000;background-color: #000;">O(n log n)</div>  
這邊