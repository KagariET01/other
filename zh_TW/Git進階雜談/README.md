# Git 進階雜談
這份講義不會拿來當社課內容，頂多當補充資料來做  
這次教學會全程使用終端機操作，請確保你看得懂下面幾行指令  
並確保你對Vim有一定的認知  
```shell
cd ..
cd git
mkdir kagariet01
rm test.txt
```
# Git Basic
社課中講到，我們可以使用GitHub Desktop很無腦的操作Git  
但UI介面能做的事情其實不多，而且Git也是從終端機出來的  
因此，我們這次要全程使用終端機  
## git clone `URL`
讓我們從0開始的GIT生活  
```shell
# 將雲端Git倉庫複製下來
git clone <URL>
git clone https://github.com/kagariet01/OJ_ans
```
其中`URL`可以從倉庫頁面>Code按鈕獲取  
(通常位於右上角)  
![Alt text](image.png)  
此指令會將Git倉庫複製下來，這個動作叫做[clone]  
## git add `path`
將檔案加入暫存器
```shell
# 只加入一個檔案
git add test.txt
# 加入某資料夾裡面的所有檔案
git add Algorithm
# 加入所有變更過的檔案
git add .
```
## git config ...
使用`git commit`之前，需要先設定名稱、郵件  
```shell
# 設定email
git config --global user.email "<email here>"
git config --global user.email "kagariet01@gmail.com"
# 設定使用者名稱
git config --global user.name "<username here>"
git config --global user.name "et01"
```
此兩個值可以任意設定，但建議以你註冊GitHub所使用的訊息設定  
## git commit
提交此次變更到本機Git倉庫
```shell
git commit
```
終端機會開啟Vim，Notepad，VS code...等文字編輯器  
該文字編輯器即為提交訊息，`#`後面的訊息會被註解掉，意即不會被寫入紀錄  
你可以在這邊簡介這次變更，形式不拘，下面示範  
```txt
Debug issue #48763
Add 48763
WA zj_a001
```
## git push `(遠端倉庫名稱)`
將變更上傳到遠端Git倉庫  
預設狀態下，`遠端倉庫名稱`免填，但後面會提到關於遠端倉庫的其他用法  
```shell
# 將變更上傳到預設遠端倉庫
git push
# 將變更上傳到名為origin的遠端倉庫
git push origin
```
## git pull `(遠端倉庫名稱)`
將遠端變更同步到本機倉庫  
```shell
# 將預設遠端倉庫的變更下載到本機倉庫
git pull
# 將名為origin，分支為main的倉庫變更下載到本機倉庫
git pull origin main
```
關於分支及遠端倉庫的用途，後面才會提到  
## git branch `分支名`
~~不會分支管理就等於不會用git~~  
使用git bracnh可以顯示目前本機分支  
```shell
git branch <branch name>
git branch new
```
你可以用git switch來切換分支
## git switch `分支名`
切換目前分支到`分支名`
```shell
git switch <branch name>
git switch new
```