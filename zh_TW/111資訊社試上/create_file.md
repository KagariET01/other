# 如何建立檔案
在桌面，點右鍵>新增>文字文件  
![](create_file.png)  
# 如何修改副檔名
檔案總管最左上角>檢視>顯示副檔名  
檔案>右鍵>重新命名  
![](re_file_1.png)  
![](re_file_2.png)  
![](re_file_3.png)  
