# HTML basic
Right, today's topic is HTML.

# Talk about myself first.
You can call me `ET01`.  
Not a person from school team.  
I'm waste.  
You can see otherthing about me from <https://kagariet01.github.io/about>.  

# What is HTML?
`HTML` is a language for Web design.  
`HTML` can do more than what I say, but you only should know this, you can learn more from [W3Schools](https://www.w3schools.com/html/default.asp).  
I think that you can't go home if I talk all about `HTML`.  

# How to start?
Open `notepad++` first(at `start`>`All app`)  
(BTW, you can use `vs code` too.)  
Click `file`>`New`  
  
![](np_create_file.png)  
  
Next, set language to `HTML` for better view.  
Click `language`>`H`>`HTML`  
  
![](./np_code_lan.png)  
  
And save the file  
Click `file`>`save as`(or `file`>`save`)  
  
![](./np_save.png)  
  
## ***It is better that set filename in English.***

# `HTML`'s basic struct
> Task: Input`HTML`'s basic struct to `.html` file.  

`ㄈ十十`have`ㄈ十十`'s basic struct, `HTML` have it, too.
```html
<!DOCTYPE html>
<html>

	<head>
	</head>
	
	<body>
	</body>

</html>
```
and now [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/HTML_basic.html)  

After you type, you can duo_click the file to see what you get. [~~Do not tell me that you don't know how to open the file.~~](你滾.md)  
  
Now you have `HTML` 's basic struct, but you still not understand any of it, but no problem, that me tell you what it mean.  
Let we see `HTML` 's basic element:  
```html
<tag_name> element </tag_name>
```  
You can see `<tag_name>` regard as  `(`  
Similarly, you can see `</tag_name>` regard as `)`  
And tag_name set the type of element  
Ex: `<html>` mean `HTML` inside. ~~(What was this man talking about?)~~  
`<head>` mean `style`, `detals` inside.  
`<body>` mean `page_element` inside, so what you see in the web is come from here.  
Remember this, I will mentioned this many time, after.  
  
And what is `<!DOCTYPE html>`mean?  
It tell browser which language you type.  
Right, browser is not as clever as you think, you have to tell him the language what you use  
~~But you will say browser is clever when you use `Google`~~  

# `Hello world`
Open the `HTML` file you write, you can see only white screen.  
Right, that is only a basic struct, we don't put anything inside.  
Let we put `Hello world` in the `HTML` file.
> Task: Show `Hello world` by using `HTML` file.

In `HTML`,we use `<p>` to define `text`.  
~~Of course, you can use it to show `Hello world`~~ ***(NONSENSE)***  
And do you know how to put `Hello world` in?  
Ringht, the ans is `<p>Hello world</p>`.  
And where should we put this?  
Tip, `<body>` mean `page_element` inside.  
The ans is below.
```html
<!DOCTYPE html>
<html>
	<head>
	</head>
	<body>
		<p>Hello world</p>
	</body>
</html>
```
> Hello world

and now [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/Hello_world.html)  
  
Tip: Remember to save the file (`[Ctrl]+[S]`), and you should refresh browser `[Ctrl]+[R]`, and that you can see what you edit.    
  
# 換行
雖然我們成功顯示一行`Hello world`，但對於小明來說，一行`Hello world`滿足不了它  
它希望你能顯示`10`行`Hello world`  
>	任務：顯示`10`行`Hello world`
>

>	`[Student]`老師，我知道，是不是J個樣子？  
>	```html
>	<!DOCTYPE html>
>	<html>
>		<head>
>		</head>
>		<body>
>			<p>
>				Hello world
>				Hello world
>				Hello world
>				Hello world
>				Hello world
>				Hello world
>				Hello world
>				Hello world
>				Hello world
>				Hello world
>			</p>
>		</body>
>	</html>
>	```
>	`[ET01]`我也不知道這樣可不可以  
>	
>	  
but [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/10Hello_world_1.html)  
  
沒意外的話你應該會得到  
> Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world

因為`HTML`在大多時候會把換行看成空格  
所以剛剛的程式碼會被看成
```html
... <p> Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world Hello world </p> ...
```
那要怎麼辦呢？  
還記得`ㄈ十十`要怎麼換行嗎， ***~~還是說你都沒在上課?~~***  
對，就是`\n`或`endl`  
在`HTML`裡也一樣，在要換行的地方加上`<br>`即可  
然後記得，只要`<br>`即可，不用上`</br>`，因為你在裡面放`元素`也沒意義  
  
and now [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/10Hello_world_2.html)  
  
答案如下  
```html
<!DOCTYPE html>
<html>
	<head>
	</head>
	<body>
		<p>
			Hello world<br>
			Hello world<br>
			Hello world<br>
			Hello world<br>
			Hello world<br>
			Hello world<br>
			Hello world<br>
			Hello world<br>
			Hello world<br>
			Hello world
		</p>
	</body>
</html>
```
> Hello world\
> Hello world\
> Hello world\
> Hello world\
> Hello world\
> Hello world\
> Hello world\
> Hello world\
> Hello world\
> Hello world
> 

# 標題
每篇文章都要有標題，網頁也一樣， ~~還是說你寫作文也都不下標題！？~~  
> 任務：為你的網頁下個標題
>
在`HTML`裡，我們使用`<h1>`~`<h6>`標籤來定義標題  
那`<h1>`和`<h2>`有甚麼差別  
`<h1>`代表最重要的標題，字體大小最大  
~~`<h2>`代表比較重要的標題，字體大小比較大  
`<h3>`代表好像很重要的標題，字體大小次大  
`<h4>`代表好像不重要的標題，字體大小次小  
`<h5>`代表比較不重要的標題，字體大小較小~~  
`<h6>`代表最不重要的標題，字體大小最小，甚至比`<p>`還要小  
and now [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/heading.html)  
> <h1>這是h1的顯示效果</h1>
> <h2>這是h2的顯示效果</h2>
> <h3>這是h3的顯示效果</h3>
> <h4>這是h4的顯示效果</h4>
> <h5>這是h5的顯示效果</h5>
> <h6>這是h6的顯示效果</h6>
> <p>這是p的顯示效果</p>
> 

# 註解
教了那麼多東西 ~~(其實也沒教多少)~~ ，程式碼應該變得很亂把  
現在你想要為你寫的東西上註解，方便讓你知道你在寫甚麼  
> 任務：新增註解，協助您看懂~~火星文~~ ***`HTML`***
>
那你還記得`ㄈ十十`要怎麼打註解嗎？  
對，就是`// P2020 here`和`/* Mozambique here */`
```c++
int a;// 我是註解 int b <-他也是註解
/* 我也是註解 他不是註解->*/int b;
```
而在`HTML`，我們使用的是`<!-- RE-45 here -->`來寫註解  
and now [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/comments.html)  
```html
<!DOCTYPE html>
<html>
	<head></head>
	<body>
		下面有註解
		<!--註解是不會顯示出來的-->
		上面有註解
	</body>
</html>
```
> 下面有註解
> <!--註解是不會顯示出來的-->
> 上面有註解

# 超連結
還記得要如何在聊天室貼`超連結`嗎?  
對，就是直接貼上去  
在`HTML`裡也是一樣......嗎？  
> 任務：新增`超連結`
>
總之 [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/link.html)  
  
不出意外的話你應該會看到連結，但是點不進去  
因為瀏覽器只知道他是一串文字，而非連結  
對，瀏覽器很笨，連超連結都要你自己定義  
~~但是你平常查`Google`的時候又會說瀏覽器好聰明~~  
那要怎麼宣告超連結?  
不賣關子了，答案是`<a>`  
and now [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/link_2.html)  
```html
<a>htpps://kagariet01.github.io/about</a>
```
> htpps://kagariet01.github.io/about
>
不出意外的話就要出意外了，他依舊不是連結  
雖然瀏覽器知道那串字是超連結，但她還是不知道要連去哪  
對，瀏覽器很笨，連超連結要連到哪都要自己定義  
~~但是你平常查`Google`的時候又會說瀏覽器好聰明~~  
  
這個時候，我們就要對`<a>`下手了，我們要把屬性`href`塞進去  
使用方法如下  
```html
<標籤 屬性="屬性值" 屬性="屬性值" ...>元素</標籤>
<a href="網址here">內容here</a>
```
這樣，`內容here`就會有`超連結`，把你帶到`網址here`  
所以剛剛的code應該要改成  
```html
<a href="https://kagariet01.github.io/about">htpps://kagariet01.github.io/about</a>
```  
> <a href="https://kagariet01.github.io/about">htpps://kagariet01.github.io/about</a>
> 

當然的，`網址here`可以和`內容here`不同  
```html
<a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">htpps://kagariet01.github.io/about</a>
```  
> <a href="https://www.youtube.com/watch?v=dQw4w9WgXcQ">htpps://kagariet01.github.io/about</a>
>

and now [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/link_3.html)  

# 圖片
現在，你還只是在寫文章而已，沒有圖片，是不會有使用者買單的  
> 任務：把`小ㄌㄌ`的圖片上去
> 
還記得要如何在聊天室貼圖片嗎？  
對，直接拖進去就好了  
但相信大家也知道，這方法在`HTML`上面是行不通的  
在`HTML`，使用`<img>`來新增圖片  
同樣，它和`<a>`一樣是需要加上屬性`src`  
```html
<img src="圖片路徑here">
<img src="https://kagariet01.github.io/loli.png">
```
請注意，`<img>`和`<a>`一樣，都不需要加`</img>`或`</a>`  
現在試著把`小ㄌㄌ`的圖片放上去，連結在這：  
`https://kagariet01.github.io/loli.png`  
`https://github.com/kagariet01/other/tree/main/111資訊社試上/loli.jpg?raw=true`  
  
and now [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/img_1.html)  
  
不出意外的話就要出意外了，剛剛邪惡的`ET01`把其中一張圖片下架了，所以瀏覽器沒辦法把其中一張圖片load出來。  
那要怎麼避免這個狀況呢？  
  
我們可以新增`alt`屬性，告訴瀏覽器，如果沒辦法載入圖片，就以`alt`屬性裡的文字替代  
```html
<img src="圖片路徑here" alt="備用文字here">
<img src="https://kagariet01.github.io/loli.png" alt="一個ㄌㄌ的照片">
```
> <img src="https://kagariet01.github.io/loli.png" alt="可愛的ㄌㄌ圖"><img src="https://raw.githubusercontent.com/KagariET01/other/main/111資訊社試上/loli.jpg" alt="可愛的ㄌㄌ圖" style="width:100px;height:100px">

and now [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/img_2.html)  
  
那這個時候你又會想問，我能不能放自己的圖片？  
答案是可以。  
  
先把你的圖片放進`.html`檔案的那個的資料夾  
然後在`src`屬性中填入該圖片的副檔名即可  
具體來講應該會這樣操作  
`[檔案結構]`  
![你的檔案結構](img_file_struct.png)  
`[你的html檔.html]`  
```html
<!DOCTYPE html>
<html>
	<body>
		<img src="你的圖片檔.jpg" alt="一個ㄌㄌ的照片">
	</body>
</html>
```
> <img src="loli.jpg" alt="一個ㄌㄌ的照片" style="width:100px;height:100px">

and now [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/img_3.html)  

# 樣式
大家應該有用過`OneBriefing`把？  
蛤？你不貲刀那是神魔東西？  
就是J個阿  
  
![](./OneBriefing.png)  
> `[Student]`老師，辣個冬冬是`PowerPoint`
> 
那你知道這是什麼嗎？  
![](./OneBriefing_Style.png)  
你知道這個東西在`Microsoft Store`上面賣`新台幣4,390`  
偷偷告訴你，HTML也有同樣的東西  
而他的售價只要`新台幣0.000`  
雖然我不是數學家，但這聽起來還不錯對吧  
對，辣個東東叫做樣式  
而他本身是一個屬性  
聽不懂對把，直接上code  
```html
<p style="color:#F00; font-size:30px;">我是顏色為紅色、字體大小為30px的文字</p>
```
> <p style="color:#F00; font-size:30px;">我是顏色為紅色、字體大小為30px的文字</p>

(但Github好像沒把樣式load出來)  
  
我們來肢解一下剛剛的程式碼  
程式碼裡有個標籤`<p>`  
`<p>`裡面有屬性`style`  
`style`的屬性值為`color:#F00; font-size:30px;`  
而我們稱`color:#F00; font-size:30px;`為該標籤(就是`<p>`)的樣式  
那我們繼續肢解樣式  
不難看出，`color:#F00; font-size:30px;`是以分號`;`來分隔的  
所以我們可以把它拆成醬子  
```css
color:#F00;
font-size:30px;
```
也不難發現，他的格式長J個樣子
```css
樣式名稱:值;
```
所以`color:#F00;`代表說標籤`<p>`的文字顏色是`#F00`，也就是紅色  
`font-size:30px;`代表說標籤`<p>`的字體大小是`30px`，也就是俗稱的30級字體  
當然的，樣式可以設定的東西也不止於此  
有興趣的人可以上W3Schools學習  
現在就讓你配配看各種顏色和各種字體的組合  
  
and now [**`let you try try`**](https://kagariet01.github.io/PCIC/2023-05-26/style_1.html)  
  
p.s.[顏色選擇器](https://www.w3schools.com/colors/colors_picker.asp)  
p.s.其實樣式`style`也是一種程式語言，叫做`css`  
p.s.其實我們寫`style`時，我們會加上換行`[Enter]`和縮排`[Tab]`，不會都寫在同一行，下面例子
```html
<p
	style="
		color:#F00;
		font-size:30px;
	"
>
	我是顏色為紅色、字體大小為30px的文字
</p>


<p	style="
	color:#F00;
	font-size:30px;
">
	我是顏色為紅色、字體大小為30px的文字
</p>
```
你可以在合適的地方加上縮排`[Tab]`和換行`[Enter]`，怎麼開心怎麼來  
但是要注意，單字中間不能加空格、換行  
***錯誤示範***
```html
<p st
yle="">
</p>
<p st yle=""></p>
```


# That's all.
And now what should I do?
